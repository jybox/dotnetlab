﻿Public Class CalcerSimple
    Private Sub 复杂ToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles 复杂ToolStripMenuItem.Click
        GroupBox1.Height = 84
        Label1.Top = 213
        TextBoxResult.Top = Label1.Top
    End Sub
    Private Sub 简单ToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles 简单ToolStripMenuItem.Click
        GroupBox1.Height = 54
        Label1.Top = 183
        TextBoxResult.Top = Label1.Top
    End Sub

    Private Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click, ButtonReduction.Click, ButtonMod.Click, ButtonMultiplication.Click, ButtonPow.Click, ButtonSub.Click, ButtonSubInteger.Click
        Try
            Select Case CType(sender, Button).Text
                Case "+"
                    TextBoxResult.Text = TextBoxA.Text + TextBoxB.Text
                Case "-"
                    TextBoxResult.Text = TextBoxA.Text - TextBoxB.Text
                Case "*"
                    TextBoxResult.Text = TextBoxA.Text * TextBoxB.Text
                Case "/"
                    TextBoxResult.Text = TextBoxA.Text / TextBoxB.Text
                Case "/(取整)"
                    TextBoxResult.Text = TextBoxA.Text \ TextBoxB.Text
                Case "^"
                    TextBoxResult.Text = TextBoxA.Text ^ TextBoxB.Text
                Case "MOD"
                    TextBoxResult.Text = TextBoxA.Text Mod TextBoxB.Text
            End Select
        Catch ex As Exception
            TextBoxResult.Text = "计算错误"
        End Try
    End Sub
End Class
