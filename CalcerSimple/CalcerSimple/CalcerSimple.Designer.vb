﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CalcerSimple
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBoxA = New System.Windows.Forms.TextBox()
        Me.TextBoxB = New System.Windows.Forms.TextBox()
        Me.TextBoxResult = New System.Windows.Forms.TextBox()
        Me.ButtonAdd = New System.Windows.Forms.Button()
        Me.ButtonReduction = New System.Windows.Forms.Button()
        Me.ButtonMultiplication = New System.Windows.Forms.Button()
        Me.ButtonSub = New System.Windows.Forms.Button()
        Me.ButtonPow = New System.Windows.Forms.Button()
        Me.ButtonMod = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.选项ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.简单ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.复杂ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ButtonSubInteger = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBoxA
        '
        Me.TextBoxA.Location = New System.Drawing.Point(131, 59)
        Me.TextBoxA.Name = "TextBoxA"
        Me.TextBoxA.Size = New System.Drawing.Size(130, 21)
        Me.TextBoxA.TabIndex = 0
        '
        'TextBoxB
        '
        Me.TextBoxB.Location = New System.Drawing.Point(131, 93)
        Me.TextBoxB.Name = "TextBoxB"
        Me.TextBoxB.Size = New System.Drawing.Size(130, 21)
        Me.TextBoxB.TabIndex = 1
        '
        'TextBoxResult
        '
        Me.TextBoxResult.Location = New System.Drawing.Point(128, 210)
        Me.TextBoxResult.Name = "TextBoxResult"
        Me.TextBoxResult.Size = New System.Drawing.Size(133, 21)
        Me.TextBoxResult.TabIndex = 2
        '
        'ButtonAdd
        '
        Me.ButtonAdd.Location = New System.Drawing.Point(6, 20)
        Me.ButtonAdd.Name = "ButtonAdd"
        Me.ButtonAdd.Size = New System.Drawing.Size(26, 23)
        Me.ButtonAdd.TabIndex = 3
        Me.ButtonAdd.Text = "+"
        Me.ButtonAdd.UseVisualStyleBackColor = True
        '
        'ButtonReduction
        '
        Me.ButtonReduction.Location = New System.Drawing.Point(38, 20)
        Me.ButtonReduction.Name = "ButtonReduction"
        Me.ButtonReduction.Size = New System.Drawing.Size(26, 23)
        Me.ButtonReduction.TabIndex = 4
        Me.ButtonReduction.Text = "-"
        Me.ButtonReduction.UseVisualStyleBackColor = True
        '
        'ButtonMultiplication
        '
        Me.ButtonMultiplication.Location = New System.Drawing.Point(70, 20)
        Me.ButtonMultiplication.Name = "ButtonMultiplication"
        Me.ButtonMultiplication.Size = New System.Drawing.Size(26, 23)
        Me.ButtonMultiplication.TabIndex = 5
        Me.ButtonMultiplication.Text = "*"
        Me.ButtonMultiplication.UseVisualStyleBackColor = True
        '
        'ButtonSub
        '
        Me.ButtonSub.Location = New System.Drawing.Point(102, 20)
        Me.ButtonSub.Name = "ButtonSub"
        Me.ButtonSub.Size = New System.Drawing.Size(26, 23)
        Me.ButtonSub.TabIndex = 6
        Me.ButtonSub.Text = "/"
        Me.ButtonSub.UseVisualStyleBackColor = True
        '
        'ButtonPow
        '
        Me.ButtonPow.Location = New System.Drawing.Point(87, 55)
        Me.ButtonPow.Name = "ButtonPow"
        Me.ButtonPow.Size = New System.Drawing.Size(26, 23)
        Me.ButtonPow.TabIndex = 7
        Me.ButtonPow.Text = "^"
        Me.ButtonPow.UseVisualStyleBackColor = True
        '
        'ButtonMod
        '
        Me.ButtonMod.Location = New System.Drawing.Point(119, 55)
        Me.ButtonMod.Name = "ButtonMod"
        Me.ButtonMod.Size = New System.Drawing.Size(35, 23)
        Me.ButtonMod.TabIndex = 8
        Me.ButtonMod.Text = "MOD"
        Me.ButtonMod.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(81, 213)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 12)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "结果"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(81, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 12)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "操作数A"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(81, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 12)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "操作数B"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.选项ToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(350, 25)
        Me.MenuStrip1.TabIndex = 13
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        '选项ToolStripMenuItem
        '
        Me.选项ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.简单ToolStripMenuItem, Me.复杂ToolStripMenuItem})
        Me.选项ToolStripMenuItem.Name = "选项ToolStripMenuItem"
        Me.选项ToolStripMenuItem.Size = New System.Drawing.Size(44, 21)
        Me.选项ToolStripMenuItem.Text = "选项"
        '
        '简单ToolStripMenuItem
        '
        Me.简单ToolStripMenuItem.Name = "简单ToolStripMenuItem"
        Me.简单ToolStripMenuItem.Size = New System.Drawing.Size(100, 22)
        Me.简单ToolStripMenuItem.Text = "简单"
        '
        '复杂ToolStripMenuItem
        '
        Me.复杂ToolStripMenuItem.Name = "复杂ToolStripMenuItem"
        Me.复杂ToolStripMenuItem.Size = New System.Drawing.Size(100, 22)
        Me.复杂ToolStripMenuItem.Text = "复杂"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ButtonSubInteger)
        Me.GroupBox1.Controls.Add(Me.ButtonAdd)
        Me.GroupBox1.Controls.Add(Me.ButtonReduction)
        Me.GroupBox1.Controls.Add(Me.ButtonMultiplication)
        Me.GroupBox1.Controls.Add(Me.ButtonSub)
        Me.GroupBox1.Controls.Add(Me.ButtonMod)
        Me.GroupBox1.Controls.Add(Me.ButtonPow)
        Me.GroupBox1.Location = New System.Drawing.Point(83, 120)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(178, 84)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "运算"
        '
        'ButtonSubInteger
        '
        Me.ButtonSubInteger.Location = New System.Drawing.Point(6, 55)
        Me.ButtonSubInteger.Name = "ButtonSubInteger"
        Me.ButtonSubInteger.Size = New System.Drawing.Size(75, 23)
        Me.ButtonSubInteger.TabIndex = 9
        Me.ButtonSubInteger.Text = "/(取整)"
        Me.ButtonSubInteger.UseVisualStyleBackColor = True
        '
        'CalcerSimple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(350, 276)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxResult)
        Me.Controls.Add(Me.TextBoxB)
        Me.Controls.Add(Me.TextBoxA)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "CalcerSimple"
        Me.Text = "CalcerSimple"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBoxA As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxResult As System.Windows.Forms.TextBox
    Friend WithEvents ButtonAdd As System.Windows.Forms.Button
    Friend WithEvents ButtonReduction As System.Windows.Forms.Button
    Friend WithEvents ButtonMultiplication As System.Windows.Forms.Button
    Friend WithEvents ButtonSub As System.Windows.Forms.Button
    Friend WithEvents ButtonPow As System.Windows.Forms.Button
    Friend WithEvents ButtonMod As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents 选项ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 简单ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 复杂ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonSubInteger As System.Windows.Forms.Button

End Class
